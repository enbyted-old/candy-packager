# candy-packager
Software for candy packing machine

# pinout
These can be remapped in _hardware.h_

* H bridge line B - PB1
* H bridge line A - PB2
* H bridge enable - PB3
* Start button - PD4
* Stop button - PD3
* Encoder at least 1 tick/s - PB0
* Position (drop off) detector - PD2
* Buzzer (not used yet) - PD1
* Speed regulation - PC4 and switch on PC5
* Reverse motion on drop off regulation - PC3 and switch on PD6
* Pedal for manual rotation - PC2
* Status LED - PB6
* Alert LED - PD7

For more information see my blog - https://enbyted.ovh