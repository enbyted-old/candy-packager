/*
 * hardware.h
 *
 * Created: 2015-02-19 08:28:52
 *  Author: Bartosz Grabias
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include <avr/interrupt.h>

/* Motor H-bridge, PWM on OCR2 */
#define MOTOR_DDR		DDRB
#define MOTOR_PORT		PORTB
#define MOTOR_A			PB2
#define MOTOR_B			PB1
#define MOTOR_EN		PB3
#define MOTOR_PWM		OCR2
#define MOTOR_FORWARD() MOTOR_PORT =  ( MOTOR_PORT & ~((1<<MOTOR_A)|(1<<MOTOR_B)) ) | (1<<MOTOR_A);
#define MOTOR_BACKWARD() MOTOR_PORT =  ( MOTOR_PORT & ~((1<<MOTOR_A)|(1<<MOTOR_B)) ) | (1<<MOTOR_B);
#define MOTOR_STOP()	MOTOR_PORT =  ( MOTOR_PORT | ((1<<MOTOR_A) | (1<<MOTOR_B)));

/* Start button, to GND */
#define START_DDR		DDRD
#define START_PORT		PORTD
#define START_PIN		PIND
#define START			PD4
#define IS_START_PRESSED() (! (START_PIN & (1<<START)) )

/* Stop button, to GND */
/* INT0 */
#define STOP_DDR		DDRD
#define STOP_PORT		PORTD
#define STOP_PIN		PIND
#define STOP			PD3
#define STOP_vect		INT1_vect
#define IS_STOP_PRESSED() (! (STOP_PIN & (1<<STOP)) )

/* Encoder on engine axis, after clutch */
/* ICP1 */
#define ENCODER_DDR		DDRB
#define ENCODER_PORT	PORTB
#define ENCODER			PB0
#define ENCODER_vect	TIMER1_CAPT_vect
#define ENCODER_OVF_vect TIMER1_COMPA_vect
#define ENCODER_OVF		OCR1A

/* Encoder on main wheel, impulse at every drop point */
/* INT1 */
#define POSITION_DDR	DDRD
#define POSITION_PORT	PORTD
#define POSITION		PD2
#define POSITION_vect	INT0_vect

/* TXD */
#define BUZZER_DDR		DDRD
#define BUZZER_PORT		PORTD
#define BUZZER			PD1
#define BUZZER_OFF()	BUZZER_PORT |= (1<<BUZZER)
#define BUZZER_ON()		BUZZER_PORT &= ~(1<<BUZZER)
// Define if buzzer is without generator
#define BUZZER_NO_GENERATOR

/* Speed regulation, SW pulled to GND on off */
#define SPEED_DDR		DDRC
#define SPEED_PORT		PORTC
#define SPEED			PC4

#define SPEED_SW_DDR	DDRC
#define SPEED_SW_PORT	PORTC
#define SPEED_SW_PIN	PINC
#define SPEED_SW		PC5

/* Backward motion regulation, SW pulled to GND on off */
#define REVERSE_DDR		DDRC
#define REVERSE_PORT	PORTC
#define REVERSE			PC3

#define PEDAL_DDR		DDRC
#define PEDAL_PORT		PORTC
#define PEDAL_PIN		PINC
#define PEDAL			PC2

#define REVERSE_SW_DDR	DDRD
#define REVERSE_SW_PORT	PORTD
#define REVERSE_SW_PIN	PIND
#define REVERSE_SW		PD6

/* Status LED, active low */
#define STATUS_DDR		DDRB
#define STATUS_PORT		PORTB
#define STATUS			PB6
#define STATUS_ON()		STATUS_PORT &= ~(1<<STATUS)
#define STATUS_OFF()	STATUS_PORT |= (1<<STATUS)

/* Alert LED, active low */
#define ALERT_DDR		DDRD
#define ALERT_PORT		PORTD
#define ALERT			PD7
#define ALERT_ON()		ALERT_PORT &= ~(1<<ALERT)
#define ALERT_OFF()		ALERT_PORT |= (1<<ALERT)

#define ADMUX_BASE		( (1<<REFS0) | (1<<ADLAR) )

#endif /* HARDWARE_H_ */