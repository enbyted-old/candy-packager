/*
 * candy_avr.c
 *
 * Created: 2015-02-18 19:17:12
 *  Author: Bartosz Grabias
 */ 

#include "hardware.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

static struct {
	uint8_t dropOffCnt;
	uint16_t wait;
} settings;

static volatile uint8_t running = 0;
static volatile uint16_t timer = 0;
static volatile uint8_t encoder = 0;
static volatile uint8_t positionHit = 0;
static volatile uint8_t adcReverse = 0;
static volatile uint8_t adcSpeed = 0;

static void init(void);
static void cycle(void);
static void dropOffOnce(void);
static void gotoNext(void);
static void waitTime(uint16_t time);
static void waitTimeOrPedal(uint16_t time);
static void waitTicks(uint8_t ticks);

int main(void)
{
	uint8_t i;
	
	cli();
	init();
	sei();
	
	STATUS_ON();
	MOTOR_STOP();
	
	while(! IS_START_PRESSED()) {
		TCNT1 = 0;
	}
	
	for(i = 0; i < 15; i++) {
		STATUS_PORT ^= (1<<STATUS);
		waitTime(10);
	}
	
	if(IS_START_PRESSED()) {
		MOTOR_FORWARD();
		ALERT_ON();
		STATUS_OFF();
		
		while(1) TCNT1 = 0;
	}
	
	running = 1;
	
	STATUS_OFF();
	ALERT_OFF();
	
	while(1) {
		/* Drop off setting potentiometer */
		if( REVERSE_SW_PIN & (1<<REVERSE_SW) ) {
			settings.dropOffCnt = 0;
		} else {
			settings.dropOffCnt = (adcReverse >> 6) + 1;
		}
		
		/* Delay setting potentiometer */
		if((SPEED_SW_PIN & (1<<SPEED_SW))) {
			settings.wait = UINT16_MAX; /* 65535 means pedal only */
		} else {
			settings.wait = (adcSpeed >> 2) * 9 + 100;
		}
		
		cycle();
	}
}

static void cycle(void) {
	uint8_t i;
	
	gotoNext();
	
	for(i = 0; i < settings.dropOffCnt; i++) {
		waitTime(20);
		dropOffOnce();
	}
		
	waitTimeOrPedal(settings.wait);
}

static void dropOffOnce(void) {
	MOTOR_FORWARD();
	waitTime(3);
	MOTOR_BACKWARD();
	waitTime(15);
	MOTOR_FORWARD();
	positionHit = 0;
	while(! positionHit)  asm __volatile__("nop");
	MOTOR_STOP();
}

static void gotoNext(void) {
	MOTOR_FORWARD();
	waitTicks(2);
	positionHit = 0;
	while(! positionHit) asm __volatile__("nop");
	MOTOR_STOP();
}

static void waitTime(uint16_t time) {
	TCNT0 = 0;
	timer = time;
	
	while(timer) TCNT1 = 0;/* keep timer 1 reset 
							  so no motor block
							  will be detected */
}

static void waitTimeOrPedal(uint16_t time) {
	TCNT0 = 0;
	timer = 10;
	while(timer) TCNT1 = 0;
	timer = time;
	
	/* wait for time to pass or for pedal to be pressed */
	while(timer && (PEDAL_PIN & (1<<PEDAL))) TCNT1 = 0; /* keep timer 1 reset 
															 so no motor block 
															 will be detected */
	timer = 0; /* reset timer in case pedal was pressed */
}

static void waitTicks(uint8_t ticks) {
	encoder = ticks;
	
	while(encoder) asm __volatile__("nop");
}

static void init(void)
{
	/* IO */
	MOTOR_DDR |= (1<<MOTOR_A) | (1<<MOTOR_B) | (1<<MOTOR_EN);
	MOTOR_PORT |= (1<<MOTOR_EN);
	MOTOR_STOP();
	
	PEDAL_DDR &= ~(1<<PEDAL);
	PEDAL_PORT |= (1<<PEDAL);
	
	START_DDR &= ~(1<<START); /* Start button, pull up */
	START_PORT |= (1<<START);
	
	STOP_DDR &= ~(1<<STOP); /* Stop button, pull up */
	STOP_PORT |= (1<<STOP);
	
	ENCODER_DDR &= (1<<ENCODER); /* Encoder 1 impulse/rotation, external pull up */
	ENCODER_PORT |= (1<<ENCODER);
	
	POSITION_DDR &= ~(1<<POSITION);  /* Position encoder 3 impulses/full rotation */
	POSITION_PORT |= (1<<POSITION); /* External pull up */
	
	BUZZER_DDR |= (1<<BUZZER); /* Signal buzzer */
	BUZZER_OFF();
	
	SPEED_DDR &= ~(1<<SPEED); /* Speed regulation */
	SPEED_PORT &= ~(1<<SPEED);
	SPEED_SW_DDR &= ~(1<<SPEED_SW);
	SPEED_SW_PORT |= (1<<SPEED_SW);
	
	REVERSE_DDR &= ~(1<<REVERSE); /* Reverse motion regulation */
	REVERSE_PORT &= ~(1<<REVERSE);
	REVERSE_SW_DDR &= ~(1<<REVERSE_SW);
	REVERSE_SW_PORT |= (1<<REVERSE_SW);
	
	STATUS_DDR |= (1<<STATUS);
	STATUS_OFF();
	
	ALERT_DDR |= (1<<ALERT);
	ALERT_OFF();
	
	/* External interrupts */
	MCUCR = (1<<ISC01) | (1<<ISC11); /* INT0 falling edge
										INT1 falling edge */
	
	GICR = (1<<INT0) | (1<<INT1); /* Enable interrups */
	
	/* Encoder, ICP1 */
	TCCR1A = 0;	/* Normal mode, clk/1024, ICP noise canceler */
	TCCR1B = (1<<ICNC1) | (1<<CS12) | (1<<CS10);
	ENCODER_OVF = 3900; /* ~ 0.5s */ //7812; // ~1s
	TIMSK = (1<<TICIE1) | (1<<OCIE1A); /* ICP interrupt and OCR1A for fail detection */
	
	/* Timer0 - system timer */
	TCCR0 = (1<<CS02) | (1<<CS00); /* clk/1024 */
	TIMSK |= (1<<TOIE0);
	
	/* ADC */
	ADMUX = ADMUX_BASE | REVERSE;
	ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1); /* clk/64 */
}

ISR(TIMER0_OVF_vect) 
{
	if(running) {
		ALERT_OFF();
		STATUS_OFF();
	}
	/* 65535 means pedal only */
	if(timer && timer < UINT16_MAX) {
		timer --;
	}
	
	/* read ADC and start next conversion */
	if(! (ADCSRA & (1<<ADSC)) ) {
		if((ADMUX & ~ADMUX_BASE) == REVERSE) {
			adcReverse = ADCH;
			ADMUX = ADMUX_BASE | SPEED;
		} else {
			adcSpeed = ADCH;
			ADMUX = ADMUX_BASE | REVERSE;
		}
		ADCSRA |= (1<<ADSC);
	}
}

ISR(ENCODER_vect)
{
	STATUS_ON();
	TCNT1 = 0;
	
	if(encoder) {
		encoder --;
	}
}

ISR(ENCODER_OVF_vect, ISR_BLOCK)
{
	TCNT1 = 0;
	ALERT_ON();
	MOTOR_BACKWARD();
	while(TCNT1 < (7812 / 5)) asm __volatile__("nop"); /* wait ~ 0.2s */
	MOTOR_FORWARD();
	TCNT1 = 0;
	while(TCNT1 < (7812 / 10)) asm __volatile__("nop"); /* wait ~ 0.1s */
	
	TCNT1 = 0;
}

ISR(POSITION_vect)
{
	ALERT_ON();
	positionHit = 1;
}

ISR(STOP_vect, ISR_NOBLOCK)
{
	_delay_ms(1);
	
	if(IS_STOP_PRESSED()) {
		/* Just restart the application, that will reset SP and everything else */
		void (*funcptr)( void ) = 0x0000;
		funcptr();
	}
}

/* Just ignore unwanted interrupts */
ISR(BADISR_vect, ISR_NAKED) {
	reti();
}